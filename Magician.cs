﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___25
{
    class Magician
    {
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string FavoriteSpell { get; private set; }

        public Magician(string name, string birthTown, string favoriteSpell)
        {
            Name = name;
            BirthTown = birthTown;
            FavoriteSpell = favoriteSpell;
        }


        public string this[string s]
        {
            get
            {
                if (s == "Name")
                    return Name;
                else if (s == "BirthTown")
                    return BirthTown;
                else if (s == "FavoriteSpell")
                    return FavoriteSpell;
                throw new Exception($"the indexer doesnt have a value for {s}");
            }
            set
            {
                if (s == "Name")
                    Name = value;
                else if (s == "BirthTown")
                    BirthTown = value;
                else if (s == "FavoriteSpell")
                    FavoriteSpell = value;
                if (s != "Name" || s != "BirthTown" || s != "FavoriteSpell")
                    throw new Exception($"the indexer doesnt have a value for {s}");
            }
        }
        public override string ToString()
        {
            return $"name: {Name}, birth town: {BirthTown}, title: {FavoriteSpell}";
        }
    }
}

