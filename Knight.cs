﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___25
{
    class Knight
    {
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string Title { get; private set; }

        public Knight(string name, string birthTown, string title)
        {
            Name = name;
            BirthTown = birthTown;
            Title = title;
        }

        public string this[string s]
        {
            get
            {
                if (s == "Name")
                    return Name;
                else if (s == "BirthTown")
                    return BirthTown;
                else if (s == "Title")
                    return Title;
                throw new Exception($"the indexer doesnt have a value for {s}");
            }
            set
            {
                if (s == "Name")
                    Name = value;
                else if (s == "BirthTown")
                    BirthTown = value;
                else if (s == "Title")
                    Title = value;
                if (s != "Name" || s != "BirthTown" || s != "Title")
                    throw new Exception($"the indexer doesnt have a value for {s}");
            }
        }

        public override string ToString()
        {
            return $"name: {Name}, birth town: {BirthTown}, title: {Title}";
        }
    }
}
