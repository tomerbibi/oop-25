﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___25
{
    class RoundTable<T> : IEnumerable<T>
    {
        private List<T> _entities = new List<T>();

        public RoundTable(List<T> entities)
        {
            _entities = entities;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _entities.GetEnumerator();
        }
        public void Add(T t)
        {
            _entities.Add(t);
        }
        public void RemoveAt(int index)
        {
            _entities.RemoveAt(index % _entities.Count);
        }
        public void clear()
        {
            _entities.Clear();
        }
        public void InsertAt(int index, T t)
        {
            _entities.Insert(index % _entities.Count, t);
        }

        public List<T> GetRounded(int amount)
        {
            List<T> t = new List<T>();
            for (int i = 0; i < amount; i++)
            {
                    t.Add(_entities[i % _entities.Count]);
            }
            return t;
        }

        public override string ToString()
        {
            string s = "";
            _entities.ForEach(item => s += $"[{item.ToString()}], ");
            return s;
        }

        public T this[int index]
        {
            get
            {
                return _entities[index];
            }
        }

        // its imposiable to do the second indexer because the enteties list is type T and i need to accsess the field Name in it...

    }
}
